<?php
require_once ("model/Product.php");

//create new product
$product = new Product(1,"Nintendo Switch", "Die aktuelle Nintendo Konsole");
//use magic getter for description;
echo $product->description."<br>";

//use magic setter for description for name
$product->name = "Nintendo Switch V1"."<br>";

//use again magic getter for name
echo $product->name."<br>";
