<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname="productsdb";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //https://www.w3schools.com/php/php_mysql_insert.asp
    //C from CRUD
//    $sql = "INSERT INTO products(name,description) VALUES (?,?);";
//    $stmt = $conn->prepare($sql);
//    $pn = "XBO";
//    $des = "The old microsoft console";
//    $stmt->bindParam(1,$pn);
//    $stmt->bindParam(2, $des);
//    $stmt->execute();
//    echo "Insert successful";
    //R for READ (one)
//    $id = 1;
//    $sql = "SELECT * FROM products WHERE id=:id";
//    $stmt = $conn->prepare($sql);
//    $stmt->bindParam(":id",$id);
//    $stmt->execute();
//    $row = $stmt->fetch();
//    if($row != null){
//        echo $row["name"]." ,".$row["description"];
//    }
    //R for READ (all)
//    $sql = "SELECT * FROM products";
//    $stmt = $conn->prepare($sql);
//    $stmt->bindParam(":id",$id);
//    $stmt->execute();
//    while(($row = $stmt->fetch()) != null){
//        echo $row["name"]." ,".$row["description"]."<br>";
//    }
    //U for UPDATE
    //$sql = "UPDATE products SET description = :newdes WHERE id=:id";
    //$stmt = $conn->prepare($sql);
    //Version 1 mit execute Parameter
    //$stmt->execute(array("newdes"=>"Neue Beschreibung cool", "id"=>1));
    //Version 2 mit bindParam
//    $newDes = "Neue Beschreibung";
//    $id = 1;
//    $stmt->bindParam(":newdes", $newDes);
//    $stmt->bindParam(":id", $id);
//    $stmt->execute();

    //D for DELETE
    //$sql = "DELETE FROM products WHERE id = :id";
    //$stmt = $conn->prepare($sql);
    //$stmt->execute(array("id"=>3));

} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
