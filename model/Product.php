<?php

require_once("DatabaseObject.php");
require_once("Database.php");

class Product implements DatabaseObject
{
    private $id;
    private $name;
    private $description;


    public function __construct($id = null, $name = null, $description = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function create()
    {
        $conn = Database::connect();
        $sql = "INSERT INTO products(name,description) VALUES (:name,:desc);";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("name" => $this->name, "desc" => $this->description));
        $this->id = $conn->lastInsertId();
        Database::disconnect();
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $conn = Database::connect();
        $sql = "UPDATE products SET description = :newdes WHERE id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("newdes" => $this->description, "id" => $this->id));
        Database::disconnect();

    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $conn = Database::connect();
        $sql = "SELECT * FROM products WHERE id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("id"=>$id));
        $row = $stmt->fetch();
        if ($row != null) {
            return new Product($id, $row["name"], $row["description"]);
        }
        Database::disconnect();
        return null;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {
        $conn = Database::connect();
        $sql = "SELECT * FROM products";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $products = [];
        while(($row = $stmt->fetch()) != null){
            $products[] = new Product( $row["id"],  $row["name"],  $row["description"]);
        }
        Database::disconnect();
        return $products;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $conn = Database::connect();
        $sql = "DELETE FROM products WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("id"=>$id));
        Database::disconnect();
    }


}
